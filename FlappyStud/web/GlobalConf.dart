/*                                                                                                                                                   
 *  Copyright (c) 2014 Matthias Gatto <uso.cosmo.ray@gmail.com>                                                                                       
 *                                                                                                                                                   
 *  This program is free software; you can redistribute it and/or modify                                                                             
 *  it under the terms of the GNU Lesser General Public License as published by                                                                             
 *  the Free Software Foundation; either version 2 of the License, or                                                                                
 *  (at your option) any later version.                                                                                                              
 *                                                                                                                                                   
 *  This program is distributed in the hope that it will be useful,                                                                                  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                    
 *  GNU General Public License for more details.                                                                                                     
 *                                                                                                                                                   
 *  You should have received a copy of the GNU Lesser General Public License                                                                                
 *  along with this program; if not, write to the Free Software                                                                                      
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.                                                                   
 *
 */

library GlobalConf;
import 'dart:js';
import 'dart:html';

/**
 * This class is used to define global conf of the game, like width, height...
 */
class GlobalConf
{
  void  setGameSize(int w, int h)
  {
    _gameScreen["width"] = w;
    _gameScreen["height"] = h;
  }
  
  void enlarge()
  {
  int oldH = gameH;
  int oldW = gameW;
  setGameSize(
        oldW * (screenH ~/ oldH)
        ,screenH);
  }
  
  void center()
  {
    int marginLeft = screenW ~/ 2 - (gameW ~/ 2);
    querySelector('#drawing').style
      ..marginLeft = marginLeft.toString() + "px";
  }
  
  int get gameW => _gameScreen["width"];
  int get gameH => _gameScreen["height"];
  int get screenW => _globalScreen['width'];
  int get screenH => _globalScreen['height'];

  static final GlobalConf _singleton = new GlobalConf._internal();

  factory GlobalConf() {
    return _singleton;
  }

  JsObject _gameScreen = context["current_screen"];
  JsObject _globalScreen = context["fenetre"];
  GlobalConf._internal();
}