/*                                                                                                                                                   
 *  Copyright (c) 2014 Matthias Gatto <uso.cosmo.ray@gmail.com>                                                                                       
 *                                                                                                                                                   
 *  This program is free software; you can redistribute it and/or modify                                                                             
 *  it under the terms of the GNU Lesser General Public License as published by                                                                             
 *  the Free Software Foundation; either version 2 of the License, or                                                                                
 *  (at your option) any later version.                                                                                                              
 *                                                                                                                                                   
 *  This program is distributed in the hope that it will be useful,                                                                                  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                    
 *  GNU General Public License for more details.                                                                                                     
 *                                                                                                                                                   
 *  You should have received a copy of the GNU Lesser General Public License                                                                                
 *  along with this program; if not, write to the Free Software                                                                                      
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.                                                                   
 *
 */

library GameLoop;
import 'dart:html';
import 'Elem.dart';
import 'GameLoopWraper.dart';
//import 'dart:async';
import 'GlobalConf.dart';

class GameLoop extends Elem {
  //var obj = new JsObject(context['GameLoop'], []);
  GameLoopWraper _glw = new GameLoopWraper();
  GameLoop(Elem nextElem) : super(nextElem)
  {
    querySelector('#drawing').hidden = true;
  }

  void  endGameLoop()
  {
    querySelector('#drawing').hidden = true;
    callNext();
  }
  
  void recursiveAnim(num time) {
    _glw.setNewPartTurn(true);
    //print("timeDiff: " + _glw.timeDiff.toString());
    _glw.setTimeDiff(time - _glw.beginTime);
    //print("timeDiff: " + _glw.timeDiff.toString());
    if (_glw.timeDiff >= 100) {
      _glw.doPartTurn(true);
      if (_glw.loose != 0) {
        return endGameLoop();
      }
      _glw.setBeginTime(_glw.beginTime + _glw.timeDiff);
      _glw.incrTurn();
    } else _glw.doPartTurn(false);
    window.animationFrame.then(recursiveAnim);
  }

  void exec() {
    //_enlarge();
    querySelector('#drawing').hidden = false;
    _gf.center();
    _glw.reset();
    _glw.init();
    window.animationFrame.then(recursiveAnim);
  }

  GlobalConf _gf = new GlobalConf();
}
