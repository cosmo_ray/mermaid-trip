/*                                                                                                                                                   
 *  Copyright (c) 2014 Matthias Gatto <uso.cosmo.ray@gmail.com>                                                                                       
 *                                                                                                                                                   
 *  This program is free software; you can redistribute it and/or modify                                                                             
 *  it under the terms of the GNU Lesser General Public License as published by                                                                             
 *  the Free Software Foundation; either version 2 of the License, or                                                                                
 *  (at your option) any later version.                                                                                                              
 *                                                                                                                                                   
 *  This program is distributed in the hope that it will be useful,                                                                                  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                    
 *  GNU General Public License for more details.                                                                                                     
 *                                                                                                                                                   
 *  You should have received a copy of the GNU Lesser General Public License                                                                                
 *  along with this program; if not, write to the Free Software                                                                                      
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.                                                                   
 *
 */

import 'GameLoop.dart';
import 'menu.dart';
import 'TextScreen.dart';

void main() {
  Menu main = new Menu();
  GameLoop gl = new GameLoop(main);
  TextScreen tx = new TextScreen(gl);
  story1(tx);
  tx.setPicture("images/Storry/xenogear-pute.jpg");
  main.addTitle('Hello Stranger !');
  main.addElem(tx, "start game");
  main.exec();
}

void story1(TextScreen tx)
{
  tx.addText("6am, My eyes is hurting me, my leg don't move as i want.\nMy arm are like a pair of iron pipe fix to my shoulder, and the alarm...\n");
}
