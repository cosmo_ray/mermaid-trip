/*                                                                                                                                                   
 *  Copyright (c) 2014 Matthias Gatto <uso.cosmo.ray@gmail.com>                                                                                       
 *                                                                                                                                                   
 *  This program is free software; you can redistribute it and/or modify                                                                             
 *  it under the terms of the GNU Lesser General Public License as published by                                                                             
 *  the Free Software Foundation; either version 2 of the License, or                                                                                
 *  (at your option) any later version.                                                                                                              
 *                                                                                                                                                   
 *  This program is distributed in the hope that it will be useful,                                                                                  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                    
 *  GNU General Public License for more details.                                                                                                     
 *                                                                                                                                                   
 *  You should have received a copy of the GNU Lesser General Public License                                                                                
 *  along with this program; if not, write to the Free Software                                                                                      
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.                                                                   
 *
 */

library Link;
import 'dart:html';
import 'Elem.dart';
import 'menu.dart';

class Link extends Elem
{
  Link(Elem nextElem, Menu menu, String txt) : super(nextElem)
  {
    _menu = menu;
    _elem = new ButtonElement();
    _elem.text = txt;
    _elem.id = 'button';
    _elem.onClick.listen(call);
  }
  
  void call(MouseEvent unused)
  {
    exec();
  }
  
  void exec()
  {
    _menu.mainDiv.remove();
    callNext();
  }
  
  Element get elem => _elem;
  
  Element _elem;
  Menu _menu;
}