/*                                                                                                                                                   
 *  Copyright (c) 2014 Matthias Gatto <uso.cosmo.ray@gmail.com>                                                                                       
 *                                                                                                                                                   
 *  This program is free software; you can redistribute it and/or modify                                                                             
 *  it under the terms of the GNU Lesser General Public License as published by                                                                             
 *  the Free Software Foundation; either version 2 of the License, or                                                                                
 *  (at your option) any later version.                                                                                                              
 *                                                                                                                                                   
 *  This program is distributed in the hope that it will be useful,                                                                                  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                    
 *  GNU General Public License for more details.                                                                                                     
 *                                                                                                                                                   
 *  You should have received a copy of the GNU Lesser General Public License                                                                                
 *  along with this program; if not, write to the Free Software                                                                                      
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.                                                                   
 *
 */

abstract class Elem
{  
  Elem(Elem nextElem, [Elem prevElem = null])
  {
    _nextElem = nextElem;
  }
  
  void exec();

  /**
   *  Methode create to use callNext with the onClick function
   */
  void callNextOC(MouseEvent)
  {
    callNext();
  }
  
  /**
   * Call the prev elem if it's not null
   */
  void callPrev()
  {
    if (_prevElem != null)
      _prevElem.exec();
  }
  
  /**
   * Call the next elem if it's not null
   */
  void callNext()
  {
    if (_nextElem != null)
      _nextElem.exec();
  }
  Elem _prevElem;
  Elem _nextElem;
}