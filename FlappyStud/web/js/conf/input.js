/*   
 *  Copyright (c) 2015 Matthias Gatto <uso.cosmo.ray@gmail.com>   
 *   
 *  This program is free software; you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License as published by 
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.  
 *   
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details. 
 *   
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software  
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.   
 *
 */

/**
 * define key
 */
function	Input()
{
    this.left = 37;
    this.right = 39;
    this.up = 38;
    this.down = 40;
    this.space = 32;
    this.tvShoot = 13;
    this.shift = 16;
    this.ctrl = 17;
    this.x = 88;
    this.c = 67;
    this.reorianteRight = this.x;
    this.reorianteLeft = this.c;
}
