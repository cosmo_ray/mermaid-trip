/*   
 *  Copyright (c) 2015 Matthias Gatto <uso.cosmo.ray@gmail.com>   
 *   
 *  This program is free software; you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License as published by 
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.  
 *   
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details. 
 *   
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software  
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.   
 *
 */

function PosSprite(x, y, w, h)
{
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.copy = function()
    {
	var ret = new PosSprite(this.x, this.y, this.w, this.h);
	return (ret);
    }
}

/* ---------- Chargement background ---------- */

var img_background_testing = new Image();
img_background_testing.src = 'images/set_1080/background/background_set.png';

/* ---------- Chargement mods ---------- */

var img_ship_testing = new Image();
img_ship_testing.src = 'images/set_1080/ia_ship/testing.svg';

/* ---------- Chargement player ---------- */

var img_player_testing = new Image();
img_player_testing.src = 'images/set_1080/player_ship/testing.png';

/* ---------- Chargement effets ---------- */

// --- Explosion
var img_explode_testing = new Image();
img_explode_testing.src = 'images/set_1080/effects/Explode_testing.png';

/* ---------- Chargement armes ---------- */

// --- Lasers
var basicSheet = new Image();
basicSheet.src = 'images/set_1080/image_set.png';
var machin_gun_pos = new PosSprite(0,50,50,50);
var laser_gun_pos = new PosSprite(50,150,50,50);
var lgatlin_gun_pos = new PosSprite(0,100,50,50);
var first_boum_pos = new PosSprite(0, 0, 43, 42);

var img_bonus = new Image();
img_bonus.src = 'images/set_1080/effects/001.gif';

/* *
 * ---------------------------------------- 
 *                                  <== OUT
 * ---------------------------------------- 
 * */
