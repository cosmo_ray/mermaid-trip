/*   
 *  Copyright (c) 2015 Matthias Gatto <uso.cosmo.ray@gmail.com>   
 *   
 *  This program is free software; you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License as published by 
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.  
 *   
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details. 
 *   
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software  
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.   
 *
 */

/* ---------- Mob Testing ---------- */

var ship_testing = addType(37, 40, img_ship_testing, 20);
var fast_ship_testing = addType(37, 40, img_ship_testing, 30);

/* ---------- Player Testing ---------- */

var player_testing = addType(60, 120, img_player_testing, 25);

/* ----------- bubble testing  ----------*/

var bubble_testing = addType(34, 37, basicSheet, 35, machin_gun_pos);
var beam = addType(14, 34, basicSheet, 35, laser_gun_pos);


/* ----------- bonus --------------------*/

var bonus = addType(34, 34, img_bonus, 35);

/*------------ Weapon Type ---------------*/

/*--------------- Effect -----------------*/

var boum = addType(37, 40, img_explode_testing, 10, first_boum_pos.copy());

/* *
 * ---------------------------------------- 
 *                                  <== OUT
 * ---------------------------------------- 
 * */
