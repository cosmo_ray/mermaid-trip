/*   
 *  Copyright (c) 2015 Matthias Gatto <uso.cosmo.ray@gmail.com>   
 *   
 *  This program is free software; you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License as published by 
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.  
 *   
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details. 
 *   
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software  
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.   
 *
 */

/* ---------- Coeur ---------- */

var secCorp = document.getElementById("corp");
if(!secCorp)
{
    console.warning("Erreur du corp");
}

var canvas = document.getElementById("drawing");
if(!canvas)
{
    console.warning("Erreur avec le canvas");
}

var drawing = canvas.getContext('2d');
if(!drawing)
{
    console.warning("Erreur avec le context du canvas");
}

/* ---------- Recuperation du window ---------- */

var fenetre = new Array();
fenetre["width"] = (window.innerWidth);
fenetre["height"] = (window.innerHeight);

/* ---------- Affichage Keon ---------- */

var drawing_keon = new Array();

drawing_keon["width"] = 480;
drawing_keon["height"] = 800;

final_size_h = fenetre["height"] * 100 / drawing_keon["height"];
secCorp.style.cssText='zoom: ' + (final_size_h - 5)  + '%';

var current_screen = canvasSize(drawing_keon);


/* *
 * ---------------------------------------- 
 *                                  <== OUT
 * ---------------------------------------- 
 * */
