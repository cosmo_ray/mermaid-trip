/*  
 *  Copyright (c) 2014 Matthias Gatto <uso.cosmo.ray@gmail.com>  
 *  
 *  This program is free software; you can redistribute it and/or modify  
 *  it under the terms of the GNU Lesser General Public License as published by  
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    
 *  GNU General Public License for more details. 
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software 
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  
 *
 */

function Monster(x, y, type, move)
{
    this.entity = new Entity(x, y, type.width, type.height, monster_type, type.speed, 2);
    this.type = type;
    this.move = move;
}

Monster.prototype.checkCol = monsterCheckCol;
Monster.prototype.removeLife = removeLife;
Monster.prototype.deathEffects = function () {
    if (Math.floor((Math.random()*100)) < 10)
	createBonus(this);
    effectManager.push(new Effect(this.entity.x, this.entity.y, boum.copy()));
    removeMob(monsterManager.indexOf(this));
}

function createBonus(monster)
{
    if (Math.floor((Math.random()*100)) < 70)
	bonusManager.push(new Bonus(bonus, giveMachinGun, monster.entity.x, monster.entity.y));
    else
	bonusManager.push(new Bonus(bonus, giveBeamRifle, monster.entity.x, monster.entity.y));
}

function removeMob(index)
{
    monsterManager.splice(index, 1);
}
