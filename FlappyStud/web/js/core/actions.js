/*   
 *  Copyright (c) 2014 Matthias Gatto <uso.cosmo.ray@gmail.com>   
 *   
 *  This program is free software; you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License as published by 
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.  
 *   
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details. 
 *   
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software  
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.   
 *
 */

/*"const" valure*/
var monster_type = 0;
var monsterBubble_type = 1;
var player_type = 2;
var playerBubble_type = 3;
var bonus_type = 4;
var effect_type = 5;

/*!"const" valure*/
var monsterManager;
var monsterBulletManager;
var playerBulletManager;
var bonusManager;
var effectManager;

var entitysManager;

function Entity(x, y, w, h, type, speed, life, oriantation)
{
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.type = type;
    this.movementUtil = new Array;
    this.speed = speed;
    this.life = life;
    if (!oriantation)
	this.oriant = new Oriantation(0, 1);
    else
	this.oriant = oriantation;
}

Entity.prototype.pCheckButomOutOfMap = function () {
    return ((this.y) > (current_screen["height"] - this.h));
};

Entity.prototype.pCheckTopOutOfMap = function () {
    return ((this.y) < 0);
};

Entity.prototype.pCheckLeftOutOfMap = function () {
    return ((this.x) < 0);
};

Entity.prototype.pCheckRightOutOfMap = function () {
    return ((this.x) > (current_screen["width"] - this.w));
};

Entity.prototype.checkButomOutOfMap = function () {
    return ((this.y) > current_screen["height"]);
};

Entity.prototype.checkTopOutOfMap = function () {
    return ((this.y + this.h) < 0);
};

Entity.prototype.checkLeftOutOfMap = function () {
    return ((this.x + this.w) < 0);
};

Entity.prototype.checkRightOutOfMap = function () {
    return ((this.x) > current_screen["width"]);
};


function simpleFire(turn, typeBubble)
{
    if (!typeBubble)
	typeBubble = bubble_testing;
    if (this.amunition <= 0 && this.amunition !== null)
	return;
    if ((turn - this.lastShoot) >  this.type.rate)
    {
	this.lastShoot = turn;
	this.amunition -= 1;
	if (this.asignAt.type == player_type)
	    var line = inversVerticalLine;
	else
	    var line = verticalLine;
	var bubble = new Bubble(typeBubble,
				line,
				this.asignAt.x + this.asignAt.w / 2 - typeBubble.width / 2,
				this.asignAt.y - typeBubble.height,
				this.type.life
			       )
	if (this.asignAt.type == player_type)
	    playerBulletManager.push(bubble);
    }
}

function removeLife(number) {
    this.entity.life -= number;
    if (this.entity.life <= 0)
	this.deathEffects();
};

/* *
 * ---------------------------------------- 
 *  <== OUT
 * ---------------------------------------- 
 * */
