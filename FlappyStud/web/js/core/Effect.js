/*  
 *  Copyright (c) 2014 Matthias Gatto <uso.cosmo.ray@gmail.com>  
 *  
 *  This program is free software; you can redistribute it and/or modify  
 *  it under the terms of the GNU Lesser General Public License as published by  
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    
 *  GNU General Public License for more details. 
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software 
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  
 *
 */

function Effect(x, y, type, sideEffect)
{
    this.entity = new Entity(x, y, type.width, type.height, effect_type, type.speed, 2);
    this.type = type;
    this.spriteDuration = 2;
    this.curSpDuration = this.spriteDuration;
    this.nbSprites = 6;
    this.curNbSprites = 1;
    if (sideEffect)
	this.sideEffect = sideEffect;
    else
	this.sideEffect = null;
    this.removeLife = function()
    {
	this.curSpDuration -= 1;
	if (this.curSpDuration == 0)
	{
	    this.curSpDuration = this.spriteDuration;
	    this.curNbSprites += 1;
	    this.type.spritePos.x += this.type.spritePos.w;
	    if (this.curNbSprites > this.nbSprites)
	    {
		effectManager.splice(effectManager.indexOf(this), 1);
		if (this.sideEffect != null)
		    this.sideEffect()
	    }
	}
    }
}
