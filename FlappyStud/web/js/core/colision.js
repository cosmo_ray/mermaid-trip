/*
 *  Copyright (c) 2014 Matthias Gatto <uso.cosmo.ray@gmail.com>  
 *
 *  This program is free software; you can redistribute it and/or modify  
 *  it under the terms of the GNU Lesser General Public License as published by  
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    
 *  GNU General Public License for more details. 
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software 
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  
 *
 */

function checkOutBorder(that) {
    return (that.entity.checkButomOutOfMap()
   || that.entity.checkTopOutOfMap()
   || that.entity.checkLeftOutOfMap()
   || that.entity.checkRightOutOfMap());
}

function pCheckOutBorder(that) {
    return (that.entity.pCheckButomOutOfMap()
   || that.entity.pCheckTopOutOfMap()
   || that.entity.pCheckLeftOutOfMap()
   || that.entity.pCheckRightOutOfMap());
}

function checkColWithMidlePlayer(monster, other) {
    return (((monster.entity.x + monster.entity.w) > (other.entity.x + other.entity.w / 2))
	    && (monster.entity.x < (other.entity.x + other.entity.w / 2))
	    && ((monster.entity.y + monster.entity.h) > (other.entity.y + other.entity.h / 2))
	    && (monster.entity.y < (other.entity.y + other.entity.h / 2)));
}

function checkColWithPlayer(monster, other) {
    return (((monster.entity.x + monster.entity.w) > (other.entity.x))
	    && (monster.entity.x < (other.entity.x + other.entity.w))
	    && ((monster.entity.y + monster.entity.h) > (other.entity.y + other.entity.h / 2))
	    && (monster.entity.y < (other.entity.y + other.entity.h / 2)));
}

function playerCheckCol() {
    if (pCheckOutBorder(this))
        return (true);
    return (false);
}

function bonusCheckCol() {
    if (checkOutBorder(this))
	return (1);
    if (checkColWithPlayer(this, player))
    {
	return (2);
    }
    return (0);
}

function monsterCheckCol() {
    if (checkOutBorder(this))
	return (1000);
    if (checkColWithMidlePlayer(this, player))
    {
	player.removeLife(this.entity.life);
	return (player.entity.life);
    }
    for (i = 0; i < playerBulletManager.length; ++i)
    {
	if (checkColWithMidlePlayer(this, playerBulletManager[i]))
	{
	    ret = playerBulletManager[i].entity.life;
        player.score += 1;
        playerBulletManager[i].removeLife(this.entity.life);
	    return (ret);
	}
    }
    return (0);
}

/* *
 * ---------------------------------------- 
 *    <== OUT
 * ---------------------------------------- 
 * */
