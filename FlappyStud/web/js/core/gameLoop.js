/*   
 *  Copyright (c) 2014 Matthias Gatto <uso.cosmo.ray@gmail.com>   
 *   
 *  This program is free software; you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License as published by 
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.  
 *   
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details. 
 *   
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software  
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.   
 *
 */

var beamRifle = new WeaponType(20, simpleFire, 2, 3, beam);
var machinGun = new WeaponType(100, simpleFire, 1, 2);
var player;

function GameLoop()
{
    var that = this;
    var audio = new Audio();
    var lenght = 1000;
    var music = true;
    var autoShoot = false;

    var playerAction = {
	x:0,
	y:0,
	shoot: autoShoot,
	init: initPlayerAction,
	exec: execPlayerAction,
	partExec : partExecPlayerAction
    };


    this.turn = 0;
    
    this.init = function(aMusic)
    {
	player = new Player(player_testing, new Weapon(beamRifle));
	player.deathEffect = this.loseGame;
	monsterManager = [];
	monsterBulletManager = [];
	playerBulletManager = [];
	bonusManager = [];
	if (aMusic)
	 music = aMusic;
	effectManager = [];
	entitysManager = new Array(monsterBulletManager, playerBulletManager, bonusManager, monsterManager, effectManager);
	this.turn = 0;
	this.loose = 0;
	beginTime = 0;

	audio.src = "./audio/" + map["audio"];
	if (!map.length || map.length == "Infinity")
	    map.length = 1000;
	audio.addEventListener('loadedmetadata', function() {
	    lenght = audio.duration;
	    if (map.randomMap)
	    {
		map.length = lenght;
		map.genMap();
	    }
	    // if you want to stop the sound just comment the line below :)
	    if (music)
	       audio.play();
	});
    }
    
    this.exec = function () {
	animFrame(recursiveAnim);
    };

    var activateKey = new Array;
    var input = new Input;
    this.loose = 0;

    function initPlayerAction()
    {
	this.x = 0;
	this.y = 0;
	this.autoShoot = 0;
    }

    var timeDiff = 0;
    this.getTimeDiff = function()
    {
	return (timeDiff);
    }
    this.setTimeDiff = function(val)
    {
	timeDiff = val;
    }
    var lastDiff = 0;
    var newPartTurn = false;
    this.getNewPartTurn = function()
    {
	return (newPartTurn);
    }
    this.setNewPartTurn = function(toSet)
    {
	newPartTurn = toSet;
    }
    var ret;
    /**
     * get pourcentage of the turn fron timeDiff
     */
    function tdGetPourcentTurn()
    {
	if (newPartTurn)
	{
	    ret = timeDiff - lastDiff;
	    if (timeDiff >= 100)
		lastDiff = 0;
	    else
		lastDiff = timeDiff;
	    newPartTurn = false;
	}
	return (ret);
    }

    function getAvancement(value, other)
    {
	return ((value * other) / 100);
    }

    /**
     * fonction call at every partTime
     */
    function partExecPlayerAction()
    {
	var old = player.entity.x;
	player.entity.x += getAvancement(this.x, tdGetPourcentTurn());
	if ((player).checkCol())
	    player.entity.x = old;
	old = player.entity.y;
	player.entity.y += getAvancement(this.y, tdGetPourcentTurn());
	if ((player).checkCol())
	    player.entity.y = old;
	if (player.isDeath)
	    return (player.deathEffects());
    }

    function execPlayerAction()
    {
	if (player.isDeath)
	    return;
	if (this.shoot)
	{
	    player.fire(that.turn, 0);
	}
    }

    /**
     * Wait for an input and store the input in key if there's one
     */
    function waitKeyDown(event)
    {
	if (event.keyCode === input.shift)
	    player.entity.speed += (player.entity.speed * 2);
	else if (event.keyCode === input.ctrl)
	    player.entity.speed -= (player.entity.speed / 2);
	else if (activateKey.indexOf(event.keyCode) === -1)
	    activateKey.push(event.keyCode);
    }

    function waitKeyUp(event)
    {
	var keyIdx = activateKey.indexOf(event.keyCode);
	if (event.keyCode === input.shift)
	    player.entity.speed = player.type.speed;
	else if (event.keyCode === input.ctrl)
	    player.entity.speed = player.type.speed;
	else
	{
	    while (keyIdx !== -1)
	    {
		activateKey.splice(keyIdx, 1);
		keyIdx = activateKey.indexOf(event.keyCode);
	    }
	}
    }

    function playerTurn(key, index, array)
    {
	switch (key)
	{
	    case input.left:
	    playerAction.x -= player.entity.speed;
	    break;
	    case input.right:
	    playerAction.x += player.entity.speed;
	    break;
	    case input.up:
	    playerAction.y -= player.entity.speed;
	    break;
	    case input.down:
	    playerAction.y += player.entity.speed;
	    break;
	    case input.space:
	    case input.tvShoot:
	    if (autoShoot) {
		autoShoot = false;
		playerAction.shoot = false;
	    } else {
		autoShoot = true;
		playerAction.shoot = true;
	    }
	    break;
	    case input.reorianteRight:
	    player.oriantationShoot = player.reorianteShoot(-10);
	    break;
	    case input.reorianteLeft:
	    player.oriantationShoot = player.reorianteShoot(10);
	    break;
	    default:
	    //console.log(key);
	    break;
	}
    }

    var monsterPartialMovementVal = 0;
    function monsterTurn(monster, index, array)
    {
    }

    function monsterPartTurn(monster, index, array)
    {
	var speed = getAvancement(monster.entity.speed, tdGetPourcentTurn());
	monster.move(this.turn, speed);
	var domage = monster.checkCol();
	if (domage !== 0)
	{
	    monster.removeLife(domage);
	    return;
	}
    }

    function bonusPartTurn(bonus, index, array)
    {
	var speed = getAvancement(bonus.entity.speed, tdGetPourcentTurn());
	bonus.move(this.turn, speed);
	var domage = bonus.checkCol();
	if (domage !== 0)
	{
	    bonus.removeLife(domage, speed);
	    return;
	}
    }

    function bonusTurn(bonus, index, array)
    {
    }

    function effectTurn(effect, index, array)
    {
	effect.removeLife();
    }

    function playerBubblePartTurn(bubble, index, array)
    {
	var speed = getAvancement(bubble.entity.speed, tdGetPourcentTurn());
	bubble.move(this.turn, speed);
    }

    function playerBubbleTurn(bubble, index, array)
    {
    }

    function nullTurn()
    {
    }

    var entitysMovementsFunctions = new Array(nullTurn, playerBubbleTurn, bonusTurn, monsterTurn, effectTurn);
    var entitysPartMovementsFunctions = new Array(nullTurn, playerBubblePartTurn, bonusPartTurn, monsterPartTurn, nullTurn);

    this.loseGame = function()
    {
	audio.pause();
	that.loose = 1;
	return 0;
    };

    this.doPartTurn = function(isTurn)
    {
	playerAction.init();
	activateKey.forEach(playerTurn);
	playerAction.partExec();
	for(var i = 0; i < entitysManager.length; ++i)
	{
	    entitysManager[i].forEach(entitysPartMovementsFunctions[i]);
	}
	if (isTurn)
	    return (doTurn());
	else
	    write_all(player);
    }

    function doTurn()
    {
	if (that.loose)
	    return ;
        
	if (beginTime >= lenght * 1000)
	{
	    that.loose = 1;
	    return;
	}
	/*player turn*/
	playerAction.exec();
	/*!player turn*/
	for(var i = 0; i < entitysManager.length; ++i)
	{
	    entitysManager[i].forEach(entitysMovementsFunctions[i]);
	}
	for (i2 = 0; i2 < map.monsterApparition[that.turn].length ;++i2) // check if ther's a monster to creat a this moment of the game
	{
	    monsterManager.push (new Monster(map.monsterApparition[that.turn][i2].x,
					     map.monsterApparition[that.turn][i2].y,
					     map.monsterApparition[that.turn][i2].type,
					     map.monsterApparition[that.turn][i2].move));
	}
	write_all();
    }

    document.addEventListener('keydown', waitKeyDown, true);
    document.addEventListener('keyup', waitKeyUp, true);

    var beginTime = 0;
    
    this.getBeginTime = function()
    {
	return (beginTime);
    }
    
    this.setBeginTime = function(val)
    {
	beginTime = val;
    }
    
    var animFrame = window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame||
	window.oRequestAnimationFrame  ||
	window.msRequestAnimationFrame ||
	null ;

    var recursiveAnim = function(time) {
	newPartTurn = true;
	timeDiff = (time - beginTime);
	if (timeDiff >= 100)
	{
	    that.doPartTurn(true);
	    console.log(that.loose);
	    if (that.loose != 0)
	    {
		return ;
	    }
	    beginTime += timeDiff;
	    ++that.turn;
	}
	else
	    that.doPartTurn(false);
	return (animFrame(recursiveAnim));
    };

}

/* *
 * ---------------------------------------- 
 *  <== OUT
 * ---------------------------------------- 
 * */
