/*   
 *  Copyright (c) 2014 Matthias Gatto <uso.cosmo.ray@gmail.com>   
 *   
 *  This program is free software; you can redistribute it and/or modify 
 *  it under the terms of the GNU Lesser General Public License as published by 
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.  
 *   
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details. 
 *   
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software  
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.   
 *
 */

/* ---------- Affichage background ("dynamique" a revoir) ---------- */
function background()
{
    drawing.strokeStyle = "#000";
    drawing.strokeRect(0, 0, current_screen["width"], current_screen["height"]);
        drawing.drawImage(img_background_testing, 0, 0, current_screen["width"], 
                          current_screen["height"]); 
}

/* ---------- Affichage mobs ---------- */
function write_all() // you change this if you want
{
    // background load
    background();
    // player load
    player_write();    
    // pnj load
    // write all monster
    for (i = 0; i < monsterManager.length; ++i)
    {
	mob_write(monsterManager[i].entity.x,
		  monsterManager[i].entity.y,
		  monsterManager[i].type);
    }
    // write all player Bubble
    for (i = 0; i < playerBulletManager.length; ++i)
    {
	mob_write(playerBulletManager[i].entity.x,
		  playerBulletManager[i].entity.y,
		  playerBulletManager[i].type);
    }
    for (i = 0; i < bonusManager.length; ++i) // write all bonus
    {
	draw_entity(bonusManager[i].entity.x,
		    bonusManager[i].entity.y,
		    bonusManager[i].type);
    }
    for (i = 0; i < effectManager.length; ++i) // write all effect
    {
	//console.log(effectManager[i].type);
	draw_entity(effectManager[i].entity.x,
		    effectManager[i].entity.y,
		    effectManager[i].type);
    }
    draw_score();
    draw_lives();
    draw_amunitions();
}


function draw_text_top(str, pos)
{
    drawing.font="22px Georgia";
    drawing.fillStyle="#FFFFFF";
    drawing.fillText(str, 10, 20 + (18 * pos));
}

function draw_amunitions()
{
    for (var i = 0; player.weapons[i]; ++i) {
	if (player.weapons[i].amunition !== null)
	    draw_text_top("munition: " + player.weapons[i].amunition,
			  2 + i);
	else
	    draw_text_top("munition: unlimited :)",
			  2 + i);

    }
}

function draw_lives()
{
    draw_text_top("Lives: " + player.entity.life, 1);
}

function draw_score()
{
    draw_text_top("Score: " + player.score, 0);
}

function draw_entity(x, y, type)
{
    //console.log(type.sprite);
    if (type.spritePos)
	drawing.drawImage(type.sprite
			  , type.spritePos.x, type.spritePos.y
			  , type.spritePos.w, type.spritePos.h
			  , x, y
			  , type.width, type.height);
    else
	drawing.drawImage(type.sprite, x, y,
			  type.width, type.height);
}

function mob_write(x, y, type)
{
    draw_entity(x, y, type);
}

function player_write()
{    
    draw_entity(player.entity.x,
		player.entity.y,
		player.type);
}

/* *
 * ---------------------------------------- 
 *                                  <== OUT
 * ---------------------------------------- 
 * */
