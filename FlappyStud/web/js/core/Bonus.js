/*  
 *  Copyright (c) 2014 Matthias Gatto <uso.cosmo.ray@gmail.com>  
 *  
 *  This program is free software; you can redistribute it and/or modify  
 *  it under the terms of the GNU Lesser General Public License as published by  
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    
 *  GNU General Public License for more details. 
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software 
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  
 *
 */

function Bonus(type, bonus, x, y)
{
    this.entity = new Entity
    (
   x,
   y,
   type.width,
   type.height,
   bonus_type,
   type.speed,
    1
    );
    this.type = type;
    this.bonus = bonus;
}

function removeBonus(index)
{
    bonusManager.splice(index, 1);
}

Bonus.prototype.move = verticalLine;
Bonus.prototype.checkCol = bonusCheckCol;
Bonus.prototype.removeLife = function (type) {
    if (type == 2)
	this.bonus();
    removeBonus(bonusManager.indexOf(this));
}

function giveMachinGun()
{
    player.giveWeapon(new Weapon(machinGun));
}

function giveBeamRifle()
{
    player.giveWeapon(new Weapon(beamRifle));
}
