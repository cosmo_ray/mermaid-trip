/*                                                                                                                                                   
 *  Copyright (c) 2014 Matthias Gatto <uso.cosmo.ray@gmail.com>                                                                                       
 *                                                                                                                                                   
 *  This program is free software; you can redistribute it and/or modify                                                                             
 *  it under the terms of the GNU Lesser General Public License as published by                                                                             
 *  the Free Software Foundation; either version 2 of the License, or                                                                                
 *  (at your option) any later version.                                                                                                              
 *                                                                                                                                                   
 *  This program is distributed in the hope that it will be useful,                                                                                  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                    
 *  GNU General Public License for more details.                                                                                                     
 *                                                                                                                                                   
 *  You should have received a copy of the GNU Lesser General Public License                                                                                
 *  along with this program; if not, write to the Free Software                                                                                      
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.                                                                   
 *
 */

library TextScreen;
import 'dart:html';
import 'Elem.dart';
//import 'Link.dart';

class TextScreen extends Elem
{
  TextScreen(nextElem) : super(nextElem)
  {
    _mainDiv = new DivElement();
    _mainDiv.id = 'TextScreen';
    _mainDiv.onClick.listen(callNextEvent);
    _picture = null;
    _mainDiv.style.color = "white";
    _mainDiv.style.textShadow = "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black";
    _mainDiv.style.fontSize = "20px";
    _mainDiv.style.textAlign = "center";
  }
  
  void _tryCreatNewDiv()
  {
    if (_dTitle != null)
      return;
    _dTitle = new DivElement();
    _dTitle.id = "txt";
    _mainDiv.insertAdjacentElement('afterBegin', _dTitle);
  }
  
  void addText(String txt)
  {
    _tryCreatNewDiv();
    _dTitle.innerHtml += txt.replaceAll("\n", "\n<br>\n");
  }
  
  void setPicture(String path)
  {
    _picture = path;
    _mainDiv.style.backgroundImage = "url(./" + path + ")";
    _mainDiv.style.height = window.screen.height.toString() + "px";
    _mainDiv.style.backgroundSize = "100% 100%";
    _mainDiv.style.backgroundRepeat = "no-repeat";
  }
  
  void callNextEvent(MouseEvent)
  {
    _mainDiv.remove();
    callNext();
  }
  
  void exec()
  {
    document.body.append(_mainDiv);
  }
  
  DivElement _dTitle = null;
  String _picture;
  DivElement _mainDiv;
}