/*                                                                                                                                                   
 *  Copyright (c) 2014 Matthias Gatto <uso.cosmo.ray@gmail.com>                                                                                       
 *                                                                                                                                                   
 *  This program is free software; you can redistribute it and/or modify                                                                             
 *  it under the terms of the GNU Lesser General Public License as published by                                                                             
 *  the Free Software Foundation; either version 2 of the License, or                                                                                
 *  (at your option) any later version.                                                                                                              
 *                                                                                                                                                   
 *  This program is distributed in the hope that it will be useful,                                                                                  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                    
 *  GNU General Public License for more details.                                                                                                     
 *                                                                                                                                                   
 *  You should have received a copy of the GNU Lesser General Public License                                                                                
 *  along with this program; if not, write to the Free Software                                                                                      
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.                                                                   
 *
 */

library menu;
import 'dart:html';
import 'Elem.dart';
import 'Link.dart';

class Menu extends Elem
{
  Menu() : super(null)
  {
    _mainDiv = new DivElement();
    _mainDiv.id = 'menu';
  }

  void addTitle(String title)
  {
    DivElement dTitle = new DivElement();
    dTitle.text = title;
    dTitle.id = "menuTitle";
    _mainDiv.insertAdjacentElement('afterBegin', dTitle);
  }

  void addLink(Link link)
  {
    links.add(link);
    _mainDiv.insertAdjacentElement('beforeEnd', link.elem);
  }

  void addElem(Elem elem, String txt)
  {
    Link link = new Link(elem, this, txt);
    addLink(link);
  }
  
  void destroy(MouseEvent unused)
  {
    _mainDiv.remove();
  }
  
  void exec()
  {
    document.body.append(_mainDiv);
  }
  
  List links = new List();
  DivElement _mainDiv;
  DivElement get mainDiv => _mainDiv;
}
