/*                                                                                                                                                   
 *  Copyright (c) 2014 Matthias Gatto <uso.cosmo.ray@gmail.com>                                                         
 *                                                                                        
 *  This program is free software; you can redistribute it and/or modify                                                                             
 *  it under the terms of the GNU Lesser General Public License as published by                                                                             
 *  the Free Software Foundation; either version 2 of the License, or                                                                                
 *  (at your option) any later version.                                                                                                              
 *                                                                                                                                                   
 *  This program is distributed in the hope that it will be useful,                                                                                  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                   
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                    
 *  GNU General Public License for more details.                                                                                                     
 *                                                                                                                                                   
 *  You should have received a copy of the GNU Lesser General Public License                                                                                
 *  along with this program; if not, write to the Free Software                                                                                      
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.                                                                   
 *
 */

library GameLoopWraper;
import 'dart:js';

class GameLoopWraper
{
  JsObject _obj = new JsObject(context['GameLoop'], []);
  bool get newPartTurn => _obj.callMethod('getNewPartTurn');
  
  void reset()
  {
    _obj = null;
    _obj = new JsObject(context['GameLoop'], []);
  }
  
  void setNewPartTurn(bool val)
  {
    _obj.callMethod('setNewPartTurn', [val]);
  }
  
  num get timeDiff => _obj.callMethod('getTimeDiff');
  
  void setTimeDiff(num val)
  {
    _obj.callMethod('setTimeDiff', [val]);
  }
  
  num get beginTime => _obj.callMethod('getBeginTime');
  
  void setBeginTime(num val)
  {
    _obj.callMethod('setBeginTime', [val]);
  }
  
  int get loose => _obj['loose'];
  int get turn => _obj['turn'];
  void incrTurn()
  {
    _obj['turn'] = _obj['turn'] + 1;
  }
  
  void init()
  {
    _obj.callMethod('init', [true]);
  }
  
  void doPartTurn(bool arg)
  {
    _obj.callMethod('doPartTurn', [arg]);
  }
}